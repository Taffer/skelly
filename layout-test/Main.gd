extends Node2D

var Log: Variant = null

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Log = get_node("AspectRatioContainer/HSplitContainer/LeftContainer/VSplitContainer/NinePatchRect/Log")
	Log.text = ""
	log_print("[i]Skelly time.[/i]")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	pass


func log_print(text: String) -> void:
	Log.append_text(text + '\n')


func _on_v_split_container_dragged(offset: int) -> void:
	log_print("[b]V Splitter[/b] dragged: " + str(offset))


func _on_h_split_container_dragged(offset: int) -> void:
	log_print("[b]H Splitter[/b] dragged: " + str(offset))
